import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';

import { Card } from '../../interfaces';
import { CardService } from '../../services/card.service';
import { AddComponent } from '../add/add.component';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  @Input() boardId: number;
  @Input() boardText: string;

  cards: Card[];

  constructor(public dialog: MatDialog, private cardService: CardService) {}

  getData(): void {
    this.cardService
      .getCards(this.boardId)
      .subscribe((cards) => (this.cards = cards));
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddComponent, {
      data: { text: '' },
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.cardService.addCard(result, this.boardId);
        this.getData();
      }
    });
  }

  drop(event: CdkDragDrop<Card[]>): void {
    const previousIndex = event.previousContainer.data.findIndex(
      (item: Card) => item === event.item.data
    );

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        previousIndex,
        event.currentIndex
      );
    }

    this.cardService.updateCardCategory(event.item.data.id, this.boardId);
  }

  ngOnInit(): void {
    this.getData();
  }
}
