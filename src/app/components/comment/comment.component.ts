import { Component, Input } from '@angular/core';

import { Comment } from '../../interfaces';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
})
export class CommentComponent {
  @Input() comment: Comment;
}
