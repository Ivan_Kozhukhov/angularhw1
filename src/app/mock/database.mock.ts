export const DATABASE = {
  boards: [
    { id: 1, text: 'To do' },
    { id: 2, text: 'In Progress' },
    { id: 3, text: 'Done' },
  ],
  cards: [
    { boardId: 1, id: 1, text: 'Do something', isLiked: false },
    { boardId: 1, id: 2, text: 'Text', isLiked: true },
    { boardId: 2, id: 3, text: 'Angular', isLiked: true },
  ],
  comments: [
    { cardId: 1, id: 1, text: 'some text' },
  ],
};
