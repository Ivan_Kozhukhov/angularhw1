import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Board } from '../interfaces';
import { DatabaseService } from './database.service';

@Injectable({
  providedIn: 'root',
})
export class BoardService {
  BOARDS: Board[];

  constructor(private databaseService: DatabaseService) {
    this.BOARDS = databaseService.get().boards;
  }

  addBoard(text: string): void {
    const newBoard: Board = {
      id: this.BOARDS.length + 1,
      text,
    };

    this.databaseService.add('boards', newBoard);
  }

  getBoards(): Observable<Board[]> {
    return of(this.BOARDS);
  }
}
