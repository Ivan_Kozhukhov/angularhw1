import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Card } from '../interfaces';
import { DatabaseService } from './database.service';

@Injectable({
  providedIn: 'root',
})
export class CardService {
  CARDS: Card[];

  constructor(private databaseService: DatabaseService) {
    this.CARDS = databaseService.get().cards;
  }

  addCard(text: string, boardId: number): void {
    const newCard: Card = {
      id: this.CARDS.length + 1,
      boardId,
      text,
      isLiked: false,
    };

    this.databaseService.add('cards', newCard);
  }

  getCardById(cardId: number): Observable<Card> {
    let card = this.CARDS.find((el) => el.id === cardId);

    if (!card) {
      card = {
        boardId: -1,
        id: -1,
        text: '',
        isLiked: false,
      };
    }

    return of(card);
  }

  updateCardRating(cardId: number): void {
    this.getCardById(cardId).subscribe((card) => {
      card.isLiked = !card.isLiked;
    });

    this.databaseService.save();
  }

  updateCardCategory(cardId: number, newBoardId: number): void {
    this.getCardById(cardId).subscribe((card) => {
      card.boardId = newBoardId;
    });

    this.databaseService.save();
  }

  getCards(boardId: number): Observable<Card[]> {
    return of(this.CARDS.filter((el) => el.boardId === boardId));
  }
}
